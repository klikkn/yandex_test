/*
Параметры
Для каждого транспорта заполняем
1) Пункт назначения
2) Пункт прибытия
-----------------
AirportBus
3) Информация о месте  (номер места или no (если места не указаны))

-----------------
Train
3) Информация о поезде
4) Информация о месте

-----------------
Flight
3) Информация о самолете
3) Информация о выходе
4) Информация о месте
5) Информация о багаже (Номер купона или auto (если трансфер))

*/

var card1 = new TrainCard("Madrid","Barcelona","78A","45B");
var card2 = new AirportBusCard("Barcelona", "Gerona Airport", "no");
var card3 = new FlightCard("Gerona Airport","Stockholm", "SK455", "45B", "3A", "344");
var card4 = new FlightCard("Stockholm","New York","SK22", "22", "7B", "auto");

cards = [
	card4,
	card3,
	card1,
	card2
];

var sort = new sortCards();
sort.printRoute(cards);