var sortCards = function() {
    var _cards = {};
    var _first = [];

    this.printRoute = function(cards) {
        if (initCards(cards)) printCorrectRoute(_first[0]);
        else console.log("Check your cards");
    };

    var initCards = function(cards) {
        var to_array = {};

        for (var i = 0; i < cards.length; i++) {
            _cards[cards[i]._from] = cards[i];
            to_array[cards[i]._to] = 1;
        }

        for (var i = 0; i < cards.length; i++) {
            var card = to_array[cards[i]._from];
            if (!card) {
                _first.push(cards[i]);
            }
        }

        if (_first.length != 1) return false;
        return true;
    };

    var printCorrectRoute = function(current) {
        current.getInformation();
        
        var next = _cards[current._to];
        if (next) {
            printCorrectRoute(next);
        }
        return;
    };
};
