var FlightCard = function(punctA, punctB, number, gate, seat, bagagge) {
    Card.apply(this, arguments);
    var _number = number;
    var _gate = gate;
    var _seat = seat;
    var _bagagge = bagagge;

    this.getInformation = function() {
        console.log("From " + this._from + " take flight " + _number + " to " + this._to + ". Gate " + _gate + ". Seat " + _seat + ". " + aboutBaggage() + ".");
    };

    var aboutBaggage = function() {
        if (_bagagge == "auto") {
            return "Baggage will be automatically transferred from your last leg";
        } else {
            return "Baggage drop at ticket counter " + _bagagge;
        }
    };
};

FlightCard.prototype = Object.create(Card.prototype);
FlightCard.prototype.constructor = FlightCard;
