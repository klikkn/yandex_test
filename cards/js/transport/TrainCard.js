var TrainCard = function(punctA, punctB, number, seat) {
    Card.apply(this, arguments);
    var _number = number;
    var _seat = seat;

    this.getInformation = function() {
        console.log("Take the train " + _number + " from " + this._from + " to " + this._to + ". Seat " + _seat + ".");
    };
};

TrainCard.prototype = Object.create(Card.prototype);
TrainCard.prototype.constructor = TrainCard;
