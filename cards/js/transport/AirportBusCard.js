var AirportBusCard = function(punctA, punctB, seat) {
    Card.apply(this, arguments);
    var _seat = seat;

    this.getInformation = function() {
        console.log("Take the airport bus from " + this._from + " to " + this._to + ". "+informationAboutSeat());
    };

    var informationAboutSeat = function() {
    	if (seat == "no") {
    		return "No seat assigment";
    	} else {
    		return "Seat "+seat+".";
    	}
    };
};

AirportBusCard.prototype = Object.create(Card.prototype);
AirportBusCard.prototype.constructor = AirportBusCard;
