function $(selector) {
    this.elems = document.querySelectorAll(selector);
}

$.prototype.hasClass = function(selector) {
    var nodeList = this.elems;
    for (var i = 0; i < nodeList.length; i++) {
        var classes = nodeList[i].getAttribute("class").split(" ");
        if (classes.indexOf(selector) == -1)
            return false;
    }
    return true;
};

$.prototype.addClass = function(selector) {
    var nodeList = this.elems;
    for (var i = 0; i < nodeList.length; i++) {
        var classes = nodeList[i].getAttribute("class").split(" ");
        if (classes.indexOf(selector) == -1) {
            classes.push(selector);
            nodeList[i].setAttribute("class", classes.join(" "));
        }
    }
};

$.prototype.removeClass = function(selector) {
    var nodeList = this.elems;
    for (var i = 0; i < nodeList.length; i++) {
        var classes = nodeList[i].getAttribute("class").split(" ");
        var selector_index = classes.indexOf(selector);
        if (selector_index > -1)
            classes.splice(selector_index);
        nodeList[i].setAttribute("class", classes.join(" "));
    }
};

$.prototype.toggleClass = function(selector) {
    var nodeList = this.elems;
    for (var i = 0; i < nodeList.length; i++) {
        var classes = nodeList[i].getAttribute("class").split(" ");
        var selector_index = classes.indexOf(selector);
        if (selector_index > -1) {
            classes.splice(selector_index);
        } else {
            classes.push(selector);
        }

        nodeList[i].setAttribute("class", classes.join(" "));
    }

};
